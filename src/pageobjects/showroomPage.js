const Helpers = require('../utilities/helpers');
const Page = require('./page');
const CarDetailsPage = require('./carDetailsPage');

class ShowroomPage extends Page{

    async open(params = '') {
        await super.open('business/showroom/' + params)
    }

    get ourVehiclesTab() { return $('[data-e2e-id="subMenuItem-/en-be/business/showroom/"]') };
    get filtersBtn() { return $$('[data-component="FilterButton"]') };
    get filterOptionsInput() { return $$('//div[@data-component="OnPageFilterBody"]//input') };
    get filterSaveBtn() { return $('//button[span[@data-key="features.showroom.filters.save"]]') };

    get vehicleCards() { return $$('//div[@data-component="VehicleCard"]') };
    get vehicleCardLabels() { return $$('//div[@data-component="VehicleCard"]//div[span[@color]]') };
    get vehicleBrands() { return $$('//div[@data-component="VehicleCard"]//h2') };
    get vehiclePrices() { return $$('//div[@data-component="VehicleCard"]//span[@data-component="Heading"]//span') };
    get viewThisCarLinks() { return $$('//div[@data-component="VehicleCard"]//a') };

    async clickOVtabIfNotActive() {
        const isActive = await Helpers.isClassHasActive(this.ourVehiclesTab);
        if (!isActive) await this.ourVehiclesTab.click();
    }

    async getFiltersBtnText() {
        return await this.filtersBtn
            .map(async function (e) {
                return await e.$('h3').getText();
            })
            .filter(Boolean)
            .map(async function (txt) {
                return await txt.toLowerCase()
            })
    }

    async applyFilter(filter, option) {
        for (const el of await this.filtersBtn) {
            let filterTxt = await el.$('h3').getText();
            filterTxt = await filterTxt!='' && await filterTxt.toLowerCase();

            if (await filterTxt === filter) {
                const filterBtn = await el.$('button');
                await filterBtn.scrollIntoView();
                await filterBtn.waitForClickable(30000);
                await filterBtn.click();
                
                if (filter === 'monthly price') {
                    const valueMin = option.split(' - ')[0];
                    const valueMax = option.split(' - ')[1];
                    
                    await this.open('?monthlyPrice=' + valueMin + ',' + valueMax);
                    await Helpers.waitForPageLoad(30000);
                    //assert filter text
                    filterTxt = await el.$('h3').getText();
                    filterTxt = await filterTxt.replace(/€/g, '');
                    await expect(filterTxt).toEqual(option);
                    break;
                } else {
                    for (const filterOption of await this.filterOptionsInput) {
                        let value = await filterOption.getAttribute('value');
                        value = await value != null && await value.toLowerCase()
                        
                        if (await value === option) {
                            const nextEl = await filterOption.nextElement();
                            await nextEl.click();
                            await this.filterSaveBtn.scrollIntoView();
                            await this.filterSaveBtn.click();
                            await Helpers.waitForPageLoad(30000);
                            break;
                        }
                    };
                }
            }
        }
    }

    async checkResultList(filterAndOption, expectedResult) {
        switch (filterAndOption) {
            case 'Popular filters - best deals':
                await this.checkResultListLabels(expectedResult);
                break;
            case 'Make & model - tesla':
                await this.checkResultListBrands(expectedResult);
                break;
            case 'Monthly price - 200 - 300':
                await this.checkResultListPrice(expectedResult);
                break;
            case 'Fuel type - electric':
                await this.checkResultListFuelTypes(expectedResult);
                break;
        
            default:
                console.log('no case; no test...')
                break;
        }
    }

    async checkResultListBrands(expectedBrand) {
        for (const brand of await this.vehicleBrands) {
            let actualBrand = await brand.getText();
            actualBrand = await actualBrand.toLowerCase();

            await expect(actualBrand).toContain(expectedBrand);
        }
    }
    async checkResultListPrice(expectedPrice) {
        for (const price of await this.vehiclePrices) {
            let actualPrice = await price.getText();
            actualPrice = await actualPrice.replace('€', '');
            actualPrice = parseInt(actualPrice);

            const i = expectedPrice.indexOf('-')
            const minPrice = parseInt(expectedPrice.substring(0, i));
            const maxPrice = parseInt(expectedPrice.substring(i + 1));

            await expect(actualPrice).toBeGreaterThanOrEqual(minPrice);
            await expect(actualPrice).toBeLessThanOrEqual(maxPrice);
        }
    }
    async checkResultListLabels(expectedLabel) {
        for (const label of await this.vehicleCardLabels) {
            let actualLabel = await label.getHTML();
            actualLabel = await actualLabel.toLowerCase();

            await expect(actualLabel).toContain(expectedLabel);
        }
    }
    async checkResultListFuelTypes(expectedFuelType) {
        //click a random element and check the fuel type
        const resultListLen = await this.vehicleCards.length
        const randomIndex = Helpers.getRandomInt(0, await resultListLen);

        await this.viewThisCarLinks[randomIndex].click();

        await Helpers.waitForPageLoad(30000);

        let actualFuelType = await CarDetailsPage.getFuelType();
        actualFuelType = await actualFuelType.toLowerCase();

        await expect(actualFuelType).toContain(expectedFuelType);
    }
}

module.exports = new ShowroomPage();
