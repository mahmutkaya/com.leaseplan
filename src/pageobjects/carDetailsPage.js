const Page = require('./page');

class CarDetailsPage extends Page {
    async open(params = '') {
        await super.open('business/our-cars/' + params)
    }

    get fuelTypeOnSingleRes() { return $('[data-key="fuel"]') };
    get fuelTypeOnMultiRes() { return $('//section[@id="Car Trimline Stack"]//h1') };


    async getFuelType(){
        let url = await browser.getUrl();
        let isMulti = await url.includes('?fuelTypes=');

        if (await isMulti) {
            await this.fuelTypeOnMultiRes.scrollIntoView();
            return await this.fuelTypeOnMultiRes.getText();
        }else{
            await this.fuelTypeOnSingleRes.scrollIntoView();
            return await this.fuelTypeOnSingleRes.getText();
        }
    }

}
 
module.exports = new CarDetailsPage();