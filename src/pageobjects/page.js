/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
module.exports = class Page {

    get acceptCookiesBtn() { return $('[title="Accept Cookies"]')}

    async open(path) {
        await browser.url(path);
        
        if (await await this.acceptCookiesBtn.isDisplayed()) {
            await this.acceptCookiesBtn.waitForClickable(30000);
            await this.acceptCookiesBtn.click();
        }
    }
}
