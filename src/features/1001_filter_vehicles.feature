@regression @filter
Feature: Filter Vehicles

    Background:
        Given I am on the showroom page
        And 'Our vehicles' tab is active
        And the following filtering options are displayed:
            | popular filters | make & model | monthly price | 60 months · 10.000 km | fuel type | more filters |


    Scenario Outline: As a user, I can list the most suitable vehicles for me by filtering

        When I use a filter option '<filter_option>'
        Then I should see the relevant list '<expected_result>'

        Examples:
            | filter_option                | expected_result                                            |
            | Popular filters - best deals | A list of vehicles with following label: best deals       |
            | Make & model - tesla         | A list containing only: Tesla                             |
            | Monthly price - 200 - 300    | A list with a monthly price between: 200 - 300            |
            | Fuel type - electric         | A list containing only with following fuel type: electric |
