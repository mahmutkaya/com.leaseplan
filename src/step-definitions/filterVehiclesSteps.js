const { Given, When, Then } = require('@cucumber/cucumber');

const ShowroomPage = require('../pageobjects/showroomPage');

let filterAndOption_;

Given('I am on the showroom page', async () => {
    await ShowroomPage.open();
});

Given('{string} tab is active', async (tab) => {
    await ShowroomPage.clickOVtabIfNotActive();
});

Given('the following filtering options are displayed:', async (dataTable) => {
    const expectedOptions = dataTable.rawTable[0];
    const actualOptions = await ShowroomPage.getFiltersBtnText();

    await expect(actualOptions).toEqual(expectedOptions);
});

When('I use a filter option {string}', async (filterAndOption) => {
    filterAndOption_ = filterAndOption;

    const i = filterAndOption.indexOf('-')
    const filter = filterAndOption.substring(0, i).trim().toLowerCase();
    const option = filterAndOption.substring(i + 1).trim().toLowerCase();

    await ShowroomPage.applyFilter(filter, option);

});

Then('I should see the relevant list {string}', async (expectedResult) => {
    const i = expectedResult.indexOf(':')
    expectedResult = expectedResult.substring(i + 1).trim().toLowerCase();

    await ShowroomPage.checkResultList(filterAndOption_, expectedResult)
});