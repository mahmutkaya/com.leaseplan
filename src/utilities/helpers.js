module.exports = class Helpers {
    /**
    * Returns true if element is active 
    */
    static async isClassHasActive(element) {
        return element
            .then(e => e.getAttribute('class'))
            .then(e => e.includes('active'));
    }

    /**
    * Returns a random number between min (inclusive) and max (exclusive)
    */
    static getRandomInt(min, max) {
        return parseInt(Math.random() * (max - min + 1) + min);
    }

    static async waitForPageLoad(time){
        await browser.waitUntil(async function () {
            const state = await browser.execute(function () {
                return document.readyState;
            });
            return state === 'complete';
        },
            {
                timeout: time,
                timeoutMsg: 'Oops! Check your internet connection'
            });
    }
}