module.exports = {
    prefs: {
        'browser.tabs.remote.autostart': false,
        'toolkit.telemetry.reportingpolicy.firstRun': false,
        'startup.homepage_welcome_url.additional': 'about:blank',
    },
    args: ['-headless']
}