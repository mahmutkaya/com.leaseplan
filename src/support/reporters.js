module.exports =
    ['spec',
        ['html-nice', {
            outputDir: './reports/html-reports/',
            filename: 'report.html',
            reportTitle: 'Test Report Title',
            //to turn on screenshots after every test
            useOnAfterCommandForScreenshot: true,
        }
        ]
    ]