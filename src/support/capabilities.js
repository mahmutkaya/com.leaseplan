const chromeOptions = require("./browserConfig/chromeOptions");
const firefoxOptions = require("./browserConfig/firefoxOptions");

const firefox = {
    browserName: 'firefox',
    'moz:firefoxOptions': firefoxOptions,
}

const chrome = {
    browserName: 'chrome',
    'goog:chromeOptions': chromeOptions,
}

let capabilities;
if (process.env.CI_JOB_NAME) {
    capabilities = [process.env.CI_JOB_NAME === 'e2e:chrome' ? chrome : firefox]
}
if (!process.env.CI) {
    capabilities = [firefox, chrome]
}

module.exports = capabilities
