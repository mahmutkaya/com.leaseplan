com.leaseplan
=================
- WEB-UI Automation Testing Using JS, webdriverIO and cucumber

- app: [leaseplan](https://www.leaseplan.com/en-be/)

### dependencies:
```properties
  "dependencies": {
    "@wdio/cli": "^7.8.0",
    "cucumber": "*",
    "eslint": "^7.31.0",
    "eslint-plugin-import": "^2.23.4",
    "wdio-html-nice-reporter": "^7.7.14"
  },
  "devDependencies": {
    "@rpii/wdio-html-reporter": "^7.7.1",
    "@wdio/cucumber-framework": "^7.8.0",
    "@wdio/local-runner": "^7.8.0",
    "@wdio/selenium-standalone-service": "^7.8.0",
    "@wdio/spec-reporter": "^7.8.0",
    "wdio-cucumber-reporter": "^0.0.2",
    "wdio-docker-service": "^3.1.2",
    "wdio-eslinter-service": "^0.0.2"
  }
```

### Setting Up
These instructions will get you a copy of the project up and running on your local machine.

- *clone the repo:*
```shell
git clone https://gitlab.com/mahmutkaya/com.leaseplan
```

### Scripts
Running tests:
```shell
npm run wdio
```
Check features and generate steps:
```shell
npm run check-steps
```
Auto-detect missing imports: [more..](https://github.com/jamesmortensen/wdio-eslinter-service)
```shell
npm run eslint
```
