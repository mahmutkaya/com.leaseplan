const path = require('path');
const capabilities = require('./src/support/capabilities');
const cucumberOpts = require('./src/support/cucumberOpts');
const hooks = require('./src/support/hooks');
const reporters = require('./src/support/reporters');
const services = require('./src/support/services');

exports.config = {
    //
    // ====================
    // Runner Configuration
    // ====================
    runner: 'local',
    // ==================
    // Specify Test Files
    // ==================
    specs: [
        './src/features/*.feature'
    ],
    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],

    maxInstances: 3,

    capabilities,
    //
    // ===================
    // Test Configurations
    // ===================
    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'error',
    outputDir: path.resolve(__dirname, './logs'),
    // If you only want to run your tests until a specific amount of tests have failed use
    // bail (default is 0 - don't bail, run all tests).
    bail: 0,

    baseUrl: 'https://www.leaseplan.com/en-be/',
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 30000,
    //
    // Default timeout in milliseconds for request
    // if browser driver or grid doesn't send response
    connectionRetryTimeout: 120000,
    //
    // Default request retries count
    connectionRetryCount: 3,

    services,

    framework: 'cucumber',

    reporters,

    cucumberOpts,
    ...hooks
}
